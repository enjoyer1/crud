CREATE FUNCTION sum_n_product(IN x varchar,IN y varchar, OUT sum boolean) AS $$
BEGIN

if(y ~* x) then
    sum := true;
else
    sum:=false;
end if;
END;
$$ LANGUAGE plpgsql;

select sum_n_product('^(\+?56)?(\s?)(0?9)(\s?)[98765]\d{7}$','990033315')
