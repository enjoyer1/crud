<?php

function llenarRegiones($conn)
{

    $salida = '';
    $sql = pg_query($conn, "SELECT * FROM region");
    $regionArray = pg_fetch_all($sql);

    foreach ($regionArray as $i => $region) {
        $salida .= "<option value=" . $region['id'] . ">" . $region['nombre'] . "</option>";
    }

    return $salida;
}


$comunas = pg_query($conn, "SELECT * FROM comuna where idregion IN(select id from region where id=1)");
$comunaArray = pg_fetch_all($comunas);


