<?php
require('config.php');


include ('data.php');



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Votacion</title>

</head>
<style>
    .container {
        padding: 4px;
    }
</style>
<body>
<div class="container ">

    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <h1>FORMULARIO DE VOTACION</h1>
            <form id="formulario" class="mt-4 p-4" style="border: 1px solid black">
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Email</label>
                    <input type="text" id="name" class="col-sm-6" name="nombre">

                </div>
                <div class="form-group row">
                    <label for="alias" class="col-sm-2 col-form-label">Alias</label>
                    <input type="text" id="alias" class="col-sm-6" name="alias">

                </div>
                <div class="form-group row">
                    <label for="rut" class="col-sm-2 col-form-label">Rut</label>
                    <input type="text" class="col-sm-6" id="rut" name="rut">

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">Email</label>
                            <input class="col-sm-6" id="email" name="email">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label>Región</label>
                    </div>
                    <div class="col-md-2 p-0">
                        <select id="selectRegion">
                            <?php echo llenarRegiones($conn); ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label>comuna</label>
                    </div>
                    <div class="col-md-2 p-0">
                        <select id="combobox2">
                            <?php foreach ($comunaArray as $i => $comuna) { ?>
                                <option value="<?php echo $comuna['id']; ?>"><?php echo $comuna["nombre"]; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label>Candidato</label>
                    </div>
                    <div class="col-md-2 p-0">
                        <select name="combo11">
                            <?php foreach ($comunaArray as $i => $comuna) { ?>
                                <option value="<?php echo $comuna['id']; ?>"><?php echo $comuna["nombre"]; ?></option>
                            <?php } ?>                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        Como nos conoció?
                    </div>
                    <div class="col-md-4">
                        <input name="checkbox[]" class="mr-6" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="mr-6" for="inlineCheckbox1">tv</label>

                        <input  name="checkbox[]" class="mr-6" type="checkbox" id="inlineCheckbox2" value="option2">
                        <label class="mr-4" for="inlineCheckbox2">texto 2</label>
                        <input name="checkbox[]" class="form-check-input mr-4" type="checkbox" id="inlineCheckbox3" value="option3">
                        <label class="form-check-label mr-4" for="inlineCheckbox3">3 </label>
                    </div>
                </div>
                <button type="submit">Enviar</button>
            </form>

            <div id="respuesta">

            </div>
        </div>
    </div>
</div>

<script src="app.js"></script>
</body>
</html>